using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.IMGUI.Controls;

namespace EasyEditorGUI.Editor
{
    public class ListView : TreeView
    {
        public event Action<int> onSelectionChanged;
        readonly IEnumerable<string> _items;
        int _id = -1;
        
        public ListView(IEnumerable<string> items) : this(new TreeViewState(), items)
        {
            
        }
        
        public ListView(TreeViewState state, IEnumerable<string> items) : base(state)
        {
            _items = items;
            showAlternatingRowBackgrounds = true;
            Reload();
        }

        protected override TreeViewItem BuildRoot()
        {
            _id = -1;
            var root = new TreeViewItem {id = _id++, depth = -1, displayName = ""};
            foreach (string item in _items)
            {
                root.AddChild(new TreeViewItem {id = _id++, depth = 0, displayName = item});
            }
            return root;
        }

        protected override bool CanMultiSelect(TreeViewItem item)
        {
            return false;
        }

        protected override void SelectionChanged(IList<int> selectedIds)
        {
            int index = selectedIds.First();
            onSelectionChanged?.Invoke(index);
        }
    }
}