using System;
using System.Collections.Generic;
using System.Linq;
using EasyEditorGUI.Runtime;
using UnityEditor;

namespace EasyEditorGUI.Editor
{
    [CustomPropertyDrawer(typeof(DropDownAttribute))]
    public class CommonDropDownDrawer : BaseDropDownDrawer
    {
        protected override string[] CreateOptions()
        {
            var attr = attribute as DropDownAttribute;
            if (!attr.enumerableType.GetInterfaces().Contains(typeof(IEnumerable<string>)))
            {
                throw new Exception("Must be IEnumerable<string> type!");
            }

            return (Activator.CreateInstance(attr.enumerableType) as IEnumerable<string>).ToArray();
        }
    }
}