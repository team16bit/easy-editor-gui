using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace EasyEditorGUI.Editor
{
    public class InspectorHubWindow : EditorWindow 
    {
        public static void Show(string title, params Type[] types)
        {
            Show(title, types.AsEnumerable());
        }

        public static void Show(string title, IEnumerable<Type> types)
        {
            var paths = types.Select(x => $"t: {x.Name}")
                .SelectMany(AssetDatabase.FindAssets)
                .Select(AssetDatabase.GUIDToAssetPath);
            Show(title, paths);
        }
        
        public static void Show(string title, params string[] settingsPaths)
        {
            Show(title, settingsPaths.AsEnumerable());
        }

        public static void Show(string title, IEnumerable<string> settingsPaths)
        {
            Show(title,settingsPaths.Select(AssetDatabase.LoadAssetAtPath<ScriptableObject>));
        }

        public static void Show(string title, params ScriptableObject[] objects)
        {
            Show(title, objects.AsEnumerable());
        }

        public static void Show(string title, IEnumerable<ScriptableObject> objects)
        {
            var win = GetWindow<InspectorHubWindow>(title);
            win._inspectorEditors = objects.Select(UnityEditor.Editor.CreateEditor);
            win.Show();
        }

        Vector2 _scrollPos;
        IEnumerable<UnityEditor.Editor> _inspectorEditors;

        void OnGUI()
        {
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
            foreach (UnityEditor.Editor editor in _inspectorEditors)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField(editor.target.name, EditorStyles.largeLabel);
                EditorGUILayout.Space();
                editor.OnInspectorGUI();    
                EditorGUILayout.EndVertical();
                EditorGUILayout.Separator();
            }
            EditorGUILayout.EndScrollView();
        }
    }
}