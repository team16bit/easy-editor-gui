using UnityEditor;
using UnityEngine;

namespace EasyEditorGUI.Editor
{
    public abstract class BaseDropDownDrawer : PropertyDrawer
    {
        string[] _optionNames;
        string[] _optionValues;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            var values = _optionValues ??= CreateOptions();
            var names = _optionNames ??= CreateDisplayNames();
            property.DropDownGUI(position, names, values);
            EditorGUI.EndProperty();
        }
        
        protected abstract string[] CreateOptions();

        protected virtual string[] CreateDisplayNames()
        {
            return _optionValues;
        }
    }
}