namespace EasyEditorGUI.Runtime
{
    public enum BitOrder
    {
        LeftToRight,
        RightToLeft
    }
}