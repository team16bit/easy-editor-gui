using System;
using UnityEngine;

namespace EasyEditorGUI.Runtime
{
    [AttributeUsage(AttributeTargets.Field)]
    public class DropDownAttribute : PropertyAttribute
    {
        public readonly Type enumerableType;

        public DropDownAttribute(Type enumerableType)
        {
            this.enumerableType = enumerableType;
        }    
    }
}