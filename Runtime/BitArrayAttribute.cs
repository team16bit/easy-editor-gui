using System;
using UnityEngine;

namespace EasyEditorGUI.Runtime
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BitArrayAttribute : PropertyAttribute
    {
        public readonly byte width;
        public bool showHexValue { get; set; }
        public bool supportSection { get; set; }
        public new BitOrder order { get; set; }

        public BitArrayAttribute(byte width)
        {
            this.width = width;
        }
    }
}